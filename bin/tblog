#!/usr/bin/python3
import argparse
import collections
from datetime import datetime, timezone
import fcntl
import json
import os
import pathlib
import re
import sys
import time


def name_from_path(path):
    if path.is_file() and path.stat().st_uid == os.getuid():
        match = re.fullmatch('tb-(.*)-.{8}\.log', path.name)
        return match and match.group(1)


def log_files(path, *, name=None):
    dir = pathlib.Path(path)
    logs_by_sandbox = collections.defaultdict(list)
    for logfile in dir.iterdir():
        actual_name = name_from_path(logfile)
        if actual_name is not None and (name is None or name == actual_name):
            logs_by_sandbox[actual_name].append(logfile)

    for logs in logs_by_sandbox.values():
        logs.sort(key=lambda p: p.stat().st_mtime, reverse=True)

    return logs_by_sandbox


def print_log(path, *, follow=False):
    with path.open() as f:
        while True:
            for line in f:
                print(line, end='')
            if not follow:
                break
            time.sleep(1)


def parse_args(args):
    parser = argparse.ArgumentParser(description='Show latest log file for given Tor Browser Sandbox.')
    list_or_list_all = parser.add_mutually_exclusive_group()
    list_or_list_all.add_argument('name', nargs='?', default='default', help='sandbox name')
    parser.add_argument('--follow', '-f', action='store_true', help='follow output')
    parser.add_argument('--list', '-l', action='store_true', help='List available logs for given sandbox.')
    list_or_list_all.add_argument('--list-all', '-L', action='store_true', help='List logs of all sandboxes.')
    parser.add_argument('--previous', '-p', type=int, default=0,
                        help='Select a previous log file. 0 is the newest logfile, 1 the second newest and so on.')
    args = parser.parse_args(args=args)
    return args


def list_logs(logs_for_sandboxes, *, all=False):
    now = datetime.utcnow().replace(microsecond=0, tzinfo=timezone.utc).astimezone()
    tz = now.tzinfo
    for name, logs in sorted(logs_for_sandboxes.items()):
        print(" Sandbox {!r} ".format(name).center(60, '-'))
        for no, log in enumerate(logs):
            mtime =  datetime.fromtimestamp(log.stat().st_mtime).replace(microsecond=0, tzinfo=tz)
            mage = now - mtime
            metadata = read_metadata(log)
            btime = bage = '-'
            start = metadata.get('start')
            if start is not None:
                btime = datetime.strptime(start, '%Y-%m-%dT%H:%M:%S.%f%z').replace(microsecond=0)
                bage = now - btime
            print('{} - {}'.format(no, log.as_posix()))
            print('    created:  {} ({} ago)'.format(btime, bage))
            print('    modified: {} ({} ago)'.format(mtime, mage))
            status = 'running' if is_running(name, metadata.get('pid')) else 'dead'
            print('    pid:      {} ({})'.format(metadata.get('pid'), status))
            print()

    print('Use -p NO to select a log file (e.g. -p 0).')


def read_metadata(path):
    try:
        with path.open() as f:
            return json.loads(f.readline())
    except json.JSONDecodeError:
        return {}


def is_running(name, pid):
    base_path = pathlib.Path('~/.local/share/tor-browser-sandboxes/').expanduser()
    lock_file = base_path / name / 'runtime' / 'sandboxed-tor-browser' / 'lock'
    pid_file = base_path / (name + '.pid')
    if not pid_file.exists():
        return False

    with pid_file.open() as f:
        running_pid = f.read().strip()

    if pid == int(running_pid) and lock_file.exists():
        try:
            with lock_file.open() as f:
                fcntl.flock(f.fileno(), fcntl.LOCK_SH|fcntl.LOCK_NB)
        except BlockingIOError:
            return True

    return False


def main(args):
    args = parse_args(args)
    if args.list_all:
        logs = log_files('/dev/shm/')
        if len(logs) == 0:
            print('No logfiles found.')
            return 1
    else:
        logs = log_files('/dev/shm/', name=args.name)
        if len(logs[args.name]) == 0:
            print('No logfile for sandbox {!r} found.'.format(args.name), file=sys.stderr)
            return 1

    if args.list or args.list_all:
        list_logs(logs, all=args.list_all)
        return 0

    paths = logs[args.name]
    if 0 <= args.previous < len(paths):
        print_log(paths[args.previous], follow=args.follow)
        return 0

    print("Can't find previous log for sandbox {!r}. No log with number {}.".format(args.name, args.previous), file=sys.stderr)
    return 1


if __name__ == '__main__':
    try:
        exit(main(sys.argv[1:]))
    except KeyboardInterrupt:
        exit(130)
