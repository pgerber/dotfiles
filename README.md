# Peter's Dotfiles

[![Build Status](https://travis-ci.org/pgerber/dotfiles.svg?branch=master)](https://travis-ci.org/pgerber/dotfiles)

Little helpers and configs I consider useful.
